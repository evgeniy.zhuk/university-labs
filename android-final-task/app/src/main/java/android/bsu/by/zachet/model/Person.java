package android.bsu.by.zachet.model;


import android.os.Parcel;
import android.os.Parcelable;

public class Person implements Parcelable {

    public int personId;
    public String name;
    public int age;

    public Person(){
        super();
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public int describeContents() {
        return 10;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeInt(this.personId);
        parcel.writeString(this.name);
        parcel.writeInt(this.age);
    }

    @Override
    public String toString() {
        return getName() + " " + getAge();
    }

    public static Parcelable.Creator<Person> CREATOR = new Parcelable.Creator<Person>() {
        @Override
        public Person createFromParcel(Parcel source) {
            Person person = new Person();

            person.personId = source.readInt();
            person.name = source.readString();
            person.age = source.readInt();

            return person;
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[0];
        }
    };
}
