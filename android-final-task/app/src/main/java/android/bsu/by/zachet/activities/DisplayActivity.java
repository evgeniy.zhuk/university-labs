package android.bsu.by.zachet.activities;

import android.app.Activity;
import android.bsu.by.zachet.R;
import android.bsu.by.zachet.model.Person;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class DisplayActivity extends Activity implements View.OnClickListener {

    private Button buttonBack;
    private TextView textViewInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_activity);

        initViews();

        Intent intent = getIntent();
        Person person = intent.getParcelableExtra("Person");
        textViewInfo.setText("Name : " + person.name + " " +
                "\nAge: " + person.age);
    }

    private void initViews() {

        buttonBack = (Button) findViewById(R.id.buttonBack);
        buttonBack.setOnClickListener(this);

        textViewInfo = (TextView) findViewById(R.id.textInfo);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.buttonBack) {
            finish();
        }
    }
}
