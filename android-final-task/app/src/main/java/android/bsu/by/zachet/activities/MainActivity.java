package android.bsu.by.zachet.activities;

import android.bsu.by.zachet.R;
import android.bsu.by.zachet.model.Person;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ListView listView;
    private ArrayList<Person> persons;
    private ArrayAdapter<Person> personArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        initViews();

    }

    private void initViews(){
        persons = new ArrayList<Person>();

        Person ivan = new Person();
        ivan.personId = 1;
        ivan.name = "Ivan";
        ivan.age = 19;

        Person boris = new Person();
        boris.personId = 2;
        boris.name = "Boris";
        boris.age = 20;

        Person Daria = new Person();
        Daria.personId = 3;
        Daria.name = "Daria";
        Daria.age = 21;

        persons.add(ivan);
        persons.add(boris);
        persons.add(Daria);

        listView = (ListView) findViewById(R.id.lvMain);
        personArrayAdapter = new ArrayAdapter<Person>(this,android.R.layout.simple_list_item_1, persons);
        listView.setAdapter(personArrayAdapter);
        listView.setOnItemClickListener(this);
        registerForContextMenu(listView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Person person = persons.get(position);
        Intent intent = new Intent(this, DisplayActivity.class);
        intent.putExtra("Person", person);
        startActivity(intent);
    }
}
