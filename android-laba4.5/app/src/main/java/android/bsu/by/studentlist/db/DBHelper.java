package android.bsu.by.studentlist.db;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, DBSchema.DB_NAME, null, DBSchema.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + DBSchema.TABLE_STUDENTS + " ("
                + DBSchema.STUDENT_ID + " integer primary key autoincrement, "
                + DBSchema.STUDENT_FIRST_NAME + " text, "
                + DBSchema.STUDENT_LAST_NAME + " text, "
                + DBSchema.STUDENT_FACULTY + " text, "
                + DBSchema.STUDENT_PHONE_NUMBER + " text, "
                + DBSchema.STUDENT_EMAIL + " text ) ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
