package android.bsu.by.studentlist.db;


public class DBSchema {

    public static final int DB_VERSION = 1;
    public static final String TABLE_STUDENTS = "students";
    public static final String DB_NAME = "studentlist.db";

    public static final String STUDENT_ID = "_id";
    public static final String STUDENT_FIRST_NAME = "f_name";
    public static final String STUDENT_LAST_NAME = "l_name";
    public static final String STUDENT_FACULTY = "faculty";
    public static final String STUDENT_PHONE_NUMBER = "phone";
    public static final String STUDENT_EMAIL = "email";
}
