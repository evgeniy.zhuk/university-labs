package android.bsu.by.studentlist.activities;

import android.bsu.by.studentlist.R;
import android.bsu.by.studentlist.constants.Constants;
import android.bsu.by.studentlist.content_provider.ContentProviderCoordinates;
import android.bsu.by.studentlist.db.DBHelper;
import android.bsu.by.studentlist.db.DBSchema;
import android.bsu.by.studentlist.model.Student;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ListView listView;
    private ArrayList<Student> students;
    private ArrayAdapter<Student> studentArrayAdapter;
    ContentResolver resolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        resolver = getContentResolver();
        initViews();
        fetchDataFromDB();

    }

    private void fetchDataFromDB() {

        students.clear();

        DBHelper dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();


        String columns[] = new String[]{
                DBSchema.STUDENT_ID,
                DBSchema.STUDENT_FIRST_NAME,
                DBSchema.STUDENT_LAST_NAME,
                DBSchema.STUDENT_FACULTY,
                DBSchema.STUDENT_PHONE_NUMBER,
                DBSchema.STUDENT_EMAIL,
        };

        Cursor cursor = db.query(DBSchema.TABLE_STUDENTS, columns, null, null, null, null, null);

        if (!cursor.isAfterLast()) {
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {

                Student student = new Student();
                student.studentId = cursor.getInt(cursor.getColumnIndex(DBSchema.STUDENT_ID));
                student.firstName = cursor.getString(cursor.getColumnIndex(DBSchema.STUDENT_FIRST_NAME));
                student.lastName = cursor.getString(cursor.getColumnIndex(DBSchema.STUDENT_LAST_NAME));
                student.faculty = cursor.getString(cursor.getColumnIndex(DBSchema.STUDENT_FACULTY));
                student.phoneNumber = cursor.getString(cursor.getColumnIndex(DBSchema.STUDENT_PHONE_NUMBER));
                student.email = cursor.getString(cursor.getColumnIndex(DBSchema.STUDENT_EMAIL));

                students.add(student);

                cursor.moveToNext();
            }
        }

        cursor.close();

    }

    private void initViews() {
        students = new ArrayList<Student>();
        listView = (ListView) findViewById(R.id.lvMain);
        studentArrayAdapter = new ArrayAdapter<Student>(this, android.R.layout.simple_list_item_1, students);
        listView.setAdapter(studentArrayAdapter);
        listView.setOnItemClickListener(this);
        registerForContextMenu(listView);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                Intent intent = new Intent(getApplicationContext(), InputActivity.class);

                startActivityForResult(intent, 0);
            }
        });

        FloatingActionButton addByCntPrvd = (FloatingActionButton) findViewById(R.id.addByCntPrvd);
        addByCntPrvd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Student student = Student.getDummyInstance();

                ContentValues values = new ContentValues();

                values.put(DBSchema.STUDENT_ID, new Random().nextInt(10 + 1));
                values.put(DBSchema.STUDENT_FIRST_NAME, student.firstName);
                values.put(DBSchema.STUDENT_LAST_NAME, student.lastName);
                values.put(DBSchema.STUDENT_FACULTY, student.faculty);
                values.put(DBSchema.STUDENT_PHONE_NUMBER, student.phoneNumber);
                values.put(DBSchema.STUDENT_EMAIL, student.email);

                Toast.makeText(getApplicationContext(), "Ivan has been created.", Toast.LENGTH_LONG).show();

                resolver.insert(ContentProviderCoordinates.CONTENT_URI_STUDENT, values);

                students.add(student);
                studentArrayAdapter.notifyDataSetChanged();
            }
        });
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;
        int itemId = adapterContextMenuInfo.position;

        menu.setHeaderTitle("Options");
        menu.add(Menu.NONE, itemId, 1, "Edit");
        menu.add(Menu.NONE, itemId, 2, "Delete");

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        int position = item.getItemId();
        final Student student = students.get(position);

        if (item.getTitle().equals("Delete")) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Question");
            builder.setMessage("Are you sure you wanna delete this contact?");

            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    DBHelper dbHelper = new DBHelper(MainActivity.this);
                    SQLiteDatabase db = dbHelper.getWritableDatabase();

                    String whereClause = String.format("%s=%d", DBSchema.STUDENT_ID, student.studentId);
                    Log.d("log", whereClause);
                    db.delete(DBSchema.TABLE_STUDENTS, whereClause, null);

                    fetchDataFromDB();
                    studentArrayAdapter.notifyDataSetChanged();
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(MainActivity.this, "Delete is canceled", Toast.LENGTH_SHORT).show();
                }
            });

            builder.create().show();

        } else if (item.getTitle().equals("Edit")) {
            Intent intent = new Intent(this, EditActivity.class);
            intent.putExtra("Student", student);
            startActivity(intent);
        }

        return super.onContextItemSelected(item);
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, InputActivity.class);
            startActivityForResult(intent, 0);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            Student student = data.getParcelableExtra("Student");
            students.add(student);
            studentArrayAdapter.notifyDataSetChanged();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();

        fetchDataFromDB();
        studentArrayAdapter.notifyDataSetChanged();
    }

    //    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
        Student student = students.get(position);
        Intent intent = new Intent(this, DisplayActivity.class);
        intent.putExtra("Student", student);
        startActivity(intent);
    }
}
