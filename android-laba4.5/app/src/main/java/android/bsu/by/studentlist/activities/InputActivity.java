package android.bsu.by.studentlist.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.bsu.by.studentlist.R;
import android.bsu.by.studentlist.db.DBHelper;
import android.bsu.by.studentlist.db.DBSchema;
import android.bsu.by.studentlist.model.Student;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;


public class InputActivity extends Activity implements View.OnClickListener {
    private EditText editFirstName;
    private EditText editLastName;
    private EditText editFaculty;
    private EditText editPhone;
    private EditText editEmail;

    private View buttonSave;
    private View buttonCancel;
    private Button buttonDate;
    private View buttonTime;
    private String birthTime;
    private String birthDate;
    private float personRating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input_activity);

        initViews();
    }

    private void initViews() {
        // Edit Text
        editFirstName = (EditText) findViewById(R.id.editFirstName);
        editLastName = (EditText) findViewById(R.id.editLastName);
        editFaculty = (EditText) findViewById(R.id.editFaculty);
        editPhone = (EditText) findViewById(R.id.editPhone);
        editEmail = (EditText) findViewById(R.id.editEmail);


        // Buttons
        buttonSave = (Button) findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(this);

        buttonCancel = (Button) findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == buttonSave) {
            Student student = new Student();

            student.firstName = editFirstName.getText().toString();
            student.lastName = editLastName.getText().toString();
            student.faculty = editFaculty.getText().toString();
            student.phoneNumber = editPhone.getText().toString();
            student.email = editEmail.getText().toString();

            DBHelper helper = new DBHelper(this);
            SQLiteDatabase db = helper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(DBSchema.STUDENT_FIRST_NAME, student.firstName);
            values.put(DBSchema.STUDENT_LAST_NAME, student.lastName);
            values.put(DBSchema.STUDENT_FACULTY, student.faculty);
            values.put(DBSchema.STUDENT_PHONE_NUMBER, student.phoneNumber);
            values.put(DBSchema.STUDENT_EMAIL, student.email);

            db.insert(DBSchema.TABLE_STUDENTS, null, values);

            Intent intent = new Intent();
            intent.putExtra("Student", student);
            setResult(0, intent);
            finish();

        } else if (v == buttonCancel) {
            finish();
        }
    }


}
