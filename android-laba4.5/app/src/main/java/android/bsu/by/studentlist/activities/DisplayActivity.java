package android.bsu.by.studentlist.activities;

import android.app.Activity;
import android.bsu.by.studentlist.R;
import android.bsu.by.studentlist.db.DBHelper;
import android.bsu.by.studentlist.db.DBSchema;
import android.bsu.by.studentlist.model.Student;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class DisplayActivity extends Activity implements View.OnClickListener {

    private Button buttonDelete;
    private Button buttonBack;
    private TextView textViewInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_activity);

        initViews();

        Intent intent = getIntent();
        Student student = intent.getParcelableExtra("Student");
        textViewInfo.setText("Name : " + student.firstName + " " +  student.lastName +
                             "\nFaculty: " + student.faculty +
                             "\nPhone number:" + student.phoneNumber +
                             "\nEmail:" + student.email );
    }

    private void initViews() {
        buttonDelete = (Button) findViewById(R.id.buttonDelete);
        buttonDelete.setOnClickListener(this);

        buttonBack = (Button) findViewById(R.id.buttonBack);
        buttonBack.setOnClickListener(this);

        textViewInfo = (TextView) findViewById(R.id.textInfo);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.buttonDelete) {
            DBHelper helper = new DBHelper(this);
            SQLiteDatabase db = helper.getWritableDatabase();

            Intent intent = getIntent();
            Student student = intent.getParcelableExtra("Student");

            String whereClause = String.format("%s = %d", DBSchema.STUDENT_ID, student.studentId);

            db.delete(DBSchema.TABLE_STUDENTS, whereClause, null);

            Toast.makeText(this, "Record is deleted :)", Toast.LENGTH_SHORT).show();
        } else if (v.getId() == R.id.buttonBack) {
            finish();
        }
    }
}
