package android.bsu.by.studentlist.content_provider;


import android.net.Uri;

public class ContentProviderCoordinates {

    public static final String AUTHORITY = "by.bsu.studentlist";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    public static final String PATH_STUDENT = "Student";
    public static final Uri CONTENT_URI_STUDENT = Uri.parse("content://" + AUTHORITY + "/" + PATH_STUDENT);
}
