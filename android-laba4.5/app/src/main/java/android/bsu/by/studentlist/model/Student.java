package android.bsu.by.studentlist.model;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.Random;

public class Student implements Parcelable {
    public int studentId;
    public String firstName;
    public String lastName;
    public String faculty;
    public String phoneNumber;
    public String email;

    public Student() {
        super();
    }

    public static Student getDummyInstance() {
        Student student = new Student();

        student.firstName = "Ivan";
        student.lastName = "Ivanov";
        student.faculty = "MMF";
        student.phoneNumber = "+375291112233";
        student.email = "ivan.ivanov@bsu.by";

        return student;
    }

    public int getStudentId() {
        return studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFaculty() {
        return faculty;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return getFirstName() + " " + getLastName();
    }

    public int describeContents() {
        return 10;
    }

    public void writeToParcel(Parcel parcel, int arg1) {
        parcel.writeInt(this.studentId);
        parcel.writeString(this.firstName);
        parcel.writeString(this.lastName);
        parcel.writeString(this.faculty);
        parcel.writeString(this.phoneNumber);
        parcel.writeString(this.email);
    }

    public static Parcelable.Creator<Student> CREATOR = new Parcelable.Creator<Student>() {
        @Override
        public Student createFromParcel(Parcel source) {
            Student student = new Student();

            student.studentId = source.readInt();
            student.firstName = source.readString();
            student.lastName = source.readString();
            student.faculty = source.readString();
            student.phoneNumber = source.readString();
            student.email = source.readString();

            return student;
        }

        @Override
        public Student[] newArray(int size) {
            return new Student[0];
        }
    };


}
