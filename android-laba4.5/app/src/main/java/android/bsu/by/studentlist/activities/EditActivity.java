package android.bsu.by.studentlist.activities;

import android.bsu.by.studentlist.R;
import android.bsu.by.studentlist.db.DBHelper;
import android.bsu.by.studentlist.db.DBSchema;
import android.bsu.by.studentlist.model.Student;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EditActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editFirstName;
    private EditText editLastName;
    private EditText editFaculty;
    private EditText editPhone;
    private EditText editEmail;
    private View buttonSave;
    private View buttonCancel;
    private Student student;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_activity);

        Intent intent = getIntent();
        student = intent.getParcelableExtra("Student");

        editFirstName = (EditText) findViewById(R.id.editFirstName);
        editLastName = (EditText) findViewById(R.id.editLastName);
        editFaculty = (EditText) findViewById(R.id.editFaculty);
        editPhone = (EditText) findViewById(R.id.editPhone);
        editEmail = (EditText) findViewById(R.id.editEmail);

        editFirstName.setText(student.firstName);
        editLastName.setText(student.lastName);
        editFaculty.setText(student.faculty);
        editPhone.setText(student.phoneNumber);
        editEmail.setText(student.email);

        // Buttons
        buttonSave = (Button) findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(this);

        buttonCancel = (Button) findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.buttonSave) {
            // update
            // update persons set name=?, address = ? where personId = 1;

            DBHelper helper = new DBHelper(this);
            SQLiteDatabase db = helper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(DBSchema.STUDENT_FIRST_NAME, editFirstName.getText().toString());
            values.put(DBSchema.STUDENT_LAST_NAME, editLastName.getText().toString());
            values.put(DBSchema.STUDENT_FACULTY, editFaculty.getText().toString());
            values.put(DBSchema.STUDENT_PHONE_NUMBER, editPhone.getText().toString());
            values.put(DBSchema.STUDENT_EMAIL, editEmail.getText().toString());

            String whereClause = String.format("%s=%d", DBSchema.STUDENT_ID, student.studentId);
            db.update(DBSchema.TABLE_STUDENTS, values, whereClause, null);
            Toast.makeText(this, "The record is updated :)", Toast.LENGTH_SHORT).show();
            finish();
        } else if (v.getId() == R.id.buttonCancel) {
            finish();
        }
    }
}
