package android.bsu.by.studentlist.content_provider;


import android.bsu.by.studentlist.db.DBHelper;
import android.bsu.by.studentlist.db.DBSchema;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

public class StudentContentProvider extends ContentProvider {

    public static final int MATCHER_CODE_STUDENT = 1;
    public static final int MATCHER_CODE_STUDENT_ID = 2;

    static UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        matcher.addURI(
                ContentProviderCoordinates.AUTHORITY,
                ContentProviderCoordinates.PATH_STUDENT,
                MATCHER_CODE_STUDENT
        );
        matcher.addURI(
                ContentProviderCoordinates.AUTHORITY,
                ContentProviderCoordinates.PATH_STUDENT + "/#",
                MATCHER_CODE_STUDENT_ID
        );
    }

    private DBHelper helper;
    private SQLiteDatabase database;

    @Override
    public boolean onCreate() {
        helper = new DBHelper(getContext());
        database = helper.getWritableDatabase();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor = null;

        int code = matcher.match(uri);
        if (code == MATCHER_CODE_STUDENT) {
            cursor = database.query(DBSchema.TABLE_STUDENTS, projection, selection, selectionArgs, null, null, sortOrder);
        }

        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        int code = matcher.match(uri);
        if (code != MATCHER_CODE_STUDENT) {
            throw new IllegalArgumentException("Wrong uri :" + uri);
        }
        long rowID = database.insert(DBSchema.TABLE_STUDENTS, null, values);
        Uri resUri = ContentUris.withAppendedId(ContentProviderCoordinates.CONTENT_URI_STUDENT, rowID);
        getContext().getContentResolver().notifyChange(resUri, null);

        return resUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        int code = matcher.match(uri);

        if(code == MATCHER_CODE_STUDENT){
            String whereClause = String.format("%s=%d", DBSchema.STUDENT_ID, selection);
            database.delete(DBSchema.TABLE_STUDENTS, whereClause, null);
        }

        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
